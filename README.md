# Hangfire.HttpJob.CSRedis

#### 介绍
基于redis储存(csredis客户端)的hangfire扩展，可注册程序集内作业、http作业、可控远程作业

#### 配置文件方式

```
{
  //定时服务监听队列，只能小写；多个以英文逗号隔开,默认为default
  "Queue": "default",
  //存储使用redis,提高性能；redis通用连接字符串
  "RedisConnection": "127.0.0.1:6379,defaultDatabase=5,poolsize=50",
  //redis key前缀
  "Prefix": "{hangfire}",
  //异常后重试次数,默认3次
  "Retry": 3,
  //服务轮询时间间隔,单位秒，默认15秒
  "SchedulePollingInterval": 15,
  //成功作业过期时间,单位小时，默认10个小时,建议不能小于1小时
  "SuccedExpireTime": 8,
  //到期时间检查间隔,单位分钟,默认30分钟
  "ExpiryCheckInterval": 30,
  //拉取超时时间,单位分钟，默认3分钟
  "FetchTimeout": 3,
  //服务实例超时时间,单位分钟，过期移除，默认2分钟
  "ServerTimeout": 2,
  //服务实例检测时间间隔,单位分钟,默认2分钟
  "ServerCheckInterval": 2,
  //http job列表(http方式调用)
  "HttpJobList": [
    {
      //唯一作业名称，不能重复
      "JobName": "",
      //作业所属队列,默认default
      "Queue": "default",
      //自定义参数，key-value只能是字符串;{ "key1": "value1" }
      "JobData": {},
      //实际调用接口服务地址，以http|https开头
      "Url": "",
      //接口请求方式：POST,GET; POST请求时，使用application/json
      "HttpMethod": "GET",
      //请求header，key-value只能是字符串,{ "key1": "value1" }
      "Header": {},
      //任务类型：1一次性任务，2周期循环任务
      "TaskType": 1,
      //任务执行延时秒数，小于等于0立即调度执行； 一次性任务有效
      "Second": 0,
      //务执行cron表达式；周期循环任务有效
      "Cron": ""
    }
  ],
  "AgentJobList": [
    {
      //唯一作业名称，不能重复
      "JobName": "",
      //作业所属队列,默认default
      "Queue": "default",
      //任务类型：1一次性任务，2周期循环任务
      "TaskType": 1,
      //任务执行延时秒数，小于等于0立即调度执行； 一次性任务有效
      "Second": 0,
      //务执行cron表达式；周期循环任务有效
      "Cron": "",
      //hangfire任务服务回调url,比如:http://localhost:5000/hangfire_redis_callback
      "ServerUrl": "",
      //实际任务执行服务url,比如:http://localhost:5000/hangfire_agent
      "AgentUrl": "",
      //自定义参数，key-value只能是字符串,{ "key1": "value1" }
      "JobData": {}
    }
  ]
}
```


#### 使用说明

```
var builder = WebApplication.CreateBuilder(args);
builder.Host.ConfigureAppConfiguration((host, config) =>
{

});
builder.Services.AddHangfireRedis(builder.Configuration);
var app = builder.Build();
app.UseHanfgireRedis();

```

更多使用方式，请看DEMO

