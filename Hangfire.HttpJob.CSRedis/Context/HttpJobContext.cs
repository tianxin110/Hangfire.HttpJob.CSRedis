﻿using System.Collections.Generic;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// http作业上下文
    /// </summary>
    public class HttpJobContext : JobContext
    {
        string queue;
        /// <summary>
        /// 作业所属队列
        /// </summary>
        public string Queue
        {
            get { return queue; }
            set { queue = string.IsNullOrWhiteSpace(value) ? "default" : value.ToLowerInvariant(); }
        }

        string url = string.Empty;
        /// <summary>
        /// http访问url
        /// </summary>
        public string Url
        {
            get { return url; }
            set { url = string.IsNullOrWhiteSpace(value) ? string.Empty : value.Trim(); }
        }

        string httpMethod = "GET";
        /// <summary>
        /// 网络请求方式:GET、POST;默认GET; POST请求时，使用application/json
        /// </summary>
        public string HttpMethod
        {
            get { return httpMethod; }
            set { httpMethod = value?.Trim()?.ToUpperInvariant() != "POST" ? "GET" : "POST"; }
        }

        /// <summary>
        /// 任务类型，默认一次性任务
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.Once;

        /// <summary>
        /// 任务执行延时秒数，小于等于0立即调度执行； 一次性任务有效
        /// </summary>
        public int Second { get; set; } = 0;

        /// <summary>
        /// 任务执行cron表达式；周期性任务有效
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// 请示header
        /// </summary>
        public Dictionary<string, string> Header { get; set; }
    }
}
