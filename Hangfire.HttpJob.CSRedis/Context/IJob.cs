﻿using System.Threading.Tasks;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// job任务接口，要执行的任务必须实现此接口,系统自动扫描添加
    /// </summary>
    public interface IJob
    {
        /// <summary>
        /// 执行作业
        /// </summary>
        /// <param name="context">作业上下文</param>
        Task ExecuteAsync(JobContext context);
    }
}
