﻿using System.Collections.Generic;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// jobcontext
    /// </summary>
    public class JobContext
    {
        /// <summary>
        /// ctor
        /// </summary>
        public JobContext() : this("")
        {

        }

        /// <summary>
        /// ctor
        /// </summary>
        public JobContext(string jobName)
        {
            JobName = jobName;
            JobData = new Dictionary<string, string>();
        }

        /// <summary>
        /// 作业类型，默认为常规作业
        /// </summary>
        public JobType JobType { get; set; } = JobType.General;

        string jobName = string.Empty;
        /// <summary>
        /// 唯一作业名称
        /// </summary>
        public string JobName
        {
            get { return jobName; }
            set { jobName = string.IsNullOrWhiteSpace(value) ? string.Empty : value.Trim(); }
        }

        /// <summary>
        /// 自定义参数
        /// </summary>
        public Dictionary<string, string> JobData { get; }
    }
}
