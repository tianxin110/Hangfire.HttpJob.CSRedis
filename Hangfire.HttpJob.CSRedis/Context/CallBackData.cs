﻿namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// 回调数据
    /// </summary>
    public class CallBackData
    {
        /// <summary>
        /// 作业ID
        /// </summary>
        public string JobId { get; set; }

        /// <summary>
        /// 是否成功执行完成
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 结果
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 执行毫秒数
        /// </summary>
        public long Duration { get; set; }
    }
}
