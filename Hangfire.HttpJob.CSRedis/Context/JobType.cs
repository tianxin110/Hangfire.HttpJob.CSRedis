﻿using System.ComponentModel;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// 作业类型
    /// </summary>
    public enum JobType
    {
        /// <summary>
        /// 常规作业
        /// </summary>
        [Description("常规作业")]
        General = 1,

        /// <summary>
        /// HTTP作业
        /// </summary>
        [Description("HTTP作业")]
        Http = 2,

        /// <summary>
        /// 远程作业
        /// </summary>
        [Description("远程作业")]
        Agent = 3
    }

    /// <summary>
    /// 任务类型
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 一次性
        /// </summary>
        [Description("一次性")]
        Once = 1,

        /// <summary>
        /// 周期性
        /// </summary>
        [Description("周期性")]
        Cycle = 2
    }
}
