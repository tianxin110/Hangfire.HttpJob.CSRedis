﻿namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// 远程作业上下文
    /// </summary>
    public class AgentJobContext : JobContext
    {
        string queue;
        /// <summary>
        /// 作业所属队列
        /// </summary>
        public string Queue
        {
            get { return queue; }
            set { queue = string.IsNullOrWhiteSpace(value) ? "default" : value.ToLowerInvariant(); }
        }

        /// <summary>
        /// 任务类型，默认一次性任务
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.Once;

        /// <summary>
        /// 任务执行延时秒数，小于等于0立即调度执行； 一次性任务有效
        /// </summary>
        public int Second { get; set; } = 0;

        /// <summary>
        /// 任务执行cron表达式；周期性任务有效
        /// </summary>
        public string Cron { get; set; }

        string serverUrl;
        /// <summary>
        /// hangfire任务服务回调url,比如:http://localhost:5000/hangfire_redis_callback
        /// </summary>
        public string ServerUrl
        {
            get { return serverUrl; }
            set
            {
                if (string.IsNullOrWhiteSpace(value) || !value.StartsWith("http"))
                    serverUrl = string.Empty;
                else
                    serverUrl = value.Trim();
            }
        }

        string agentUrl;
        /// <summary>
        /// 实际任务执行服务url,比如:http://localhost:5000/hangfire_agent
        /// </summary>
        public string AgentUrl
        {
            get { return agentUrl; }
            set
            {
                if (string.IsNullOrWhiteSpace(value) || !value.StartsWith("http"))
                    agentUrl = string.Empty;
                else
                    agentUrl = value.Trim();
            }
        }

        /// <summary>
        /// 任务执行类名,比如:Microsoft.AspNetCore.Builder.WebApplication,Microsoft.AspNetCore
        /// </summary>
        public string AssemblyQualifiedName { get; set; }

        /// <summary>
        /// 作业任务ID
        /// </summary>
        public string TaskId { get; set; }
    }
}
