﻿using System;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// job，默认为一次性作业
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class JobAttribute : Attribute
    {
        /// <summary>
        /// 唯一作业名称
        /// </summary>
        public string JobName { get; set; }
    }
}
