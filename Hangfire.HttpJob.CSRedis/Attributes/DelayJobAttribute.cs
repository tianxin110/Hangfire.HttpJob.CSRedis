﻿using System;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// DelayJobAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DelayJobAttribute : JobAttribute
    {
        /// <summary>
        /// 延时执行时间
        /// </summary>
        public TimeSpan Time { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="second">延时执行时间,秒数</param>
        public DelayJobAttribute(int second)
        {
            Time = TimeSpan.FromSeconds(second);
        }
    }
}