﻿using System;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// RecurringJob
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class RecurringJobAttribute : JobAttribute
    {
        /// <summary>
        /// 任务队列,默认为default
        /// </summary>
        public string Queue { get; set; } = "default";

        /// <summary>
        /// cro表达式,比如: 0 15 10 * * ?
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="cron">cro表达式,比如: 0 15 10 * * ?</param>
        public RecurringJobAttribute(string cron) : this(cron, "default")
        {
        }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="cron">cro表达式,比如: 0 15 10 * * ?</param>
        /// <param name="queue">任务队列,默认为default</param>
        public RecurringJobAttribute(string cron, string queue)
        {
            Cron = cron;
            Queue = queue;
        }
    }
}