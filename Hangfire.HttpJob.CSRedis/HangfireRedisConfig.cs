﻿using System;
using System.Collections.Generic;

namespace Hangfire.HttpJob.CSRedis
{
    /// <summary>
    /// hangfire redis配置
    /// </summary>
    public class HangfireRedisConfig
    {
        /// <summary>
        /// 监听队列,只能小写；多个以英文逗号隔开,默认为default
        /// </summary>
        public string Queue { get; set; } = "default";

        /// <summary>
        /// 监听队列
        /// </summary>
        public List<string> QueueList
        {
            get
            {
                var list = new List<string>();
                if (string.IsNullOrWhiteSpace(Queue))
                    list.Add("default");
                else
                {
                    var tempArray = Queue.Split(',', StringSplitOptions.RemoveEmptyEntries);
                    foreach (var queue in tempArray)
                    {
                        list.Add(queue.ToLowerInvariant());
                    }

                    if (!list.Contains("default"))
                        list.Add("default");
                }

                return list;
            }
        }

        private string redisConnection = "127.0.0.1:6379, defaultDatabase = 10, poolsize = 50";
        /// <summary>
        /// redis连接字符串
        /// </summary>
        public string RedisConnection
        {
            get { return redisConnection; }
            set { redisConnection = string.IsNullOrWhiteSpace(value) ? redisConnection : value; }
        }

        private string prefix = "{hangfire}";
        /// <summary>
        /// 前缀,默认{hangfire}
        /// </summary>
        public string Prefix
        {
            get { return prefix; }
            set { prefix = string.IsNullOrWhiteSpace(value) ? prefix : value; }
        }

        private int retry = 3;
        /// <summary>
        /// 异常后重试次数,默认3次
        /// </summary>
        public int Retry
        {
            get { return retry; }
            set { retry = value <= 0 ? 3 : value; }
        }

        private int serverCheckInterval = 2;
        /// <summary>
        /// 服务实例检测时间间隔,单位分钟,默认2分钟
        /// </summary>
        public int ServerCheckInterval
        {
            get { return serverCheckInterval; }
            set { serverCheckInterval = value < 2 ? 2 : value; }
        }

        private int serverTimeout = 2;
        /// <summary>
        /// 服务实例超时时间,单位分钟，过期移除，默认2分钟
        /// </summary>
        public int ServerTimeout
        {
            get { return serverTimeout; }
            set { serverTimeout = value < 2 ? 2 : value; }
        }

        private int schedulePollingInterval = 15;
        /// <summary>
        /// 服务轮询时间间隔,单位秒，默认15秒
        /// </summary>
        public int SchedulePollingInterval
        {
            get { return schedulePollingInterval; }
            set { schedulePollingInterval = value < 1 ? 1 : value; }
        }

        private int succedExpireTime = 10;
        /// <summary>
        /// 成功作业过期时间,单位小时，默认10个小时,建议不能小于1小时
        /// </summary>
        public int SuccedExpireTime
        {
            get { return succedExpireTime; }
            set { succedExpireTime = value < 1 ? 1 : value; }
        }

        private int fetchTimeout = 3;
        /// <summary>
        /// 拉取超时时间,单位分钟，默认3分钟
        /// </summary>
        public int FetchTimeout
        {
            get { return fetchTimeout; }
            set { fetchTimeout = value < 2 ? 2 : value; }
        }

        private int expiryCheckInterval = 30;
        /// <summary>
        /// 到期时间检查间隔,单位分钟,默认30分钟
        /// </summary>
        public int ExpiryCheckInterval
        {
            get { return expiryCheckInterval; }
            set { expiryCheckInterval = value < 20 ? 20 : value; }
        }

        /// <summary>
        /// http job 列表
        /// </summary>
        public List<HttpJobContext> HttpJobList { get; set; }

        /// <summary>
        /// 远程agent job列表,可控制实际执行状态
        /// </summary>
        public List<AgentJobContext> AgentJobList { get; set; }
    }
}