﻿namespace Hangfire.HttpJob.CSRedis.JobAgent.Sample
{
    public class TestAgentJob : IAgentJob
    {
        public string JobName { get; set; } = "test_agent_001";

        public async Task<string> ExectuteAsync(AgentJobContext context)
        {
            await Task.Delay(2000 * 60);
            return await Task.FromResult("我执行完成了");
        }
    }
}