﻿using System.Threading.Tasks;

namespace Hangfire.HttpJob.CSRedis.JobAgent
{
    /// <summary>
    /// agent job 
    /// </summary>
    public interface IAgentJob
    {
        /// <summary>
        /// 唯一作业名称，与作业服务端配置的远程作业名称一致且唯一
        /// </summary>
        string JobName { get; set; }

        /// <summary>
        /// 执行作业
        /// </summary>
        Task<string> ExectuteAsync(AgentJobContext context);
    }
}
